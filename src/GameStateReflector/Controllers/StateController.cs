﻿using Microsoft.AspNet.Mvc;

namespace GameStateReflector.Controllers
{
    using Microsoft.AspNet.Cors;
    using Microsoft.Extensions.Caching.Memory;

    using Newtonsoft.Json.Linq;

    [EnableCors("AllowAny")]
    [Route("api/[controller]")]
    public class StateController : Controller
    {
        private readonly IMemoryCache memoryCache;

        public StateController(IMemoryCache memoryCache)
        {
            this.memoryCache = memoryCache;
        }

        [HttpPost]
        [Route("state")]
        public JToken SetStateAndReturnQueryResult([FromBody]JToken jToken)
        {
            memoryCache.Set(jToken.Value<string>("source"), jToken.Value<JObject>("state"));
            return memoryCache.Get<JToken>(jToken.Value<string>("query"));
        }

        [HttpGet]
        [Route("state")]
        public JToken GetState(string key)
        {
            return memoryCache.Get<JToken>(key);
        }
    }
}
